import path from 'path';
import VueLoaderPlugin from 'vue-loader/lib/plugin';

export default {
	entry: [
		'babel-polyfill',
		path.resolve(__dirname, 'src/main.js')
	],
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader',
				]
			}, {
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					'file-loader'
				]
			}, {
				test: /\.(png|svg|jpg|gif|jpeg)$/,
				use: [
					'file-loader'
				]
			}, {
				test: /\.vue$/,
				use: [
					'vue-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		]
	},

	resolve: {
		alias: {
			'@': path.resolve(__dirname, 'src'),
		}
	},

	plugins: [
		new VueLoaderPlugin(),
	],

	presets: [
	    '@vue/cli-plugin-babel/preset'
	]
};
