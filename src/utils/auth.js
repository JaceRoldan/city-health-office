import axios from 'axios';

const API_URL = process.env.VUE_APP_API_URL || 'http://localhost:8000/';

const headers = () => {
	return {
		'Content-Type': 'application/json',
		'Authorization': 'Token ' + window.localStorage.getItem('token')
	};
};

const methods = {
	get_pk: 'get_pk',
	filter: 'filter',
	create: 'create',
	update: 'update',
	remove: 'delete'
};

function resolveBaseURL(endpoint, method) {
	let resolvedMethod = method.toLowerCase() in methods ?
		methods[method] : 'Error: Invalid method';
	let resolvedURL =  API_URL + 'api/' + endpoint + '/'
		+ resolvedMethod + '/';
	return resolvedURL;
}

const get_pk = async (endpoint, id) => {
	let requestHeaders = headers();
	let options = {
		headers: requestHeaders
	};

	if (!requestHeaders) {
		return 'Error: Cannot access headers!';
	}

	if (!requestHeaders.Authorization) {
		return 'Error: No permissions to access City Health API';
	}

	let url = resolveBaseURL(endpoint, 'get_pk') + id;
	let response = await axios.get(url, options);
	return response;
};

const filter = async (endpoint, filters) => {
	if (!filters)
		filters = {};

	let requestHeaders = headers();
	let options = {
		headers: requestHeaders
	};

	if (!requestHeaders) {
		return 'Error: Cannot access headers!';
	}

	if (!requestHeaders.Authorization) {
		return 'Error: No permissions to access City Health API';
	}

	let paramedUrl = resolveBaseURL(endpoint, 'filter');

	let resolvedFilters = Object.keys(filters).map((filterKey) => {
		return `${filterKey}=${filters[filterKey]}`;
	});

	paramedUrl += resolvedFilters.length ? 
		'?' + resolvedFilters.join('&') :
		'';

	let response = await axios.get(paramedUrl, options);
	return response;
};

const create = async (endpoint, data) => {
	let requestHeaders = headers();
	let options = {
		headers: requestHeaders
	};

	if (!requestHeaders) {
		return 'Error: Cannot access headers!';
	}

	if (!requestHeaders.Authorization) {
		return 'Error: No permissions to access City Health API';
	}

	let url = resolveBaseURL(endpoint, 'create');

	let response = await axios.post(url, data, options);
	return response;

};

const update = async (endpoint, filters, data) => {
	let requestHeaders = headers();
	let options = {
		headers: requestHeaders
	};

	if (!requestHeaders) {
		return 'Error: Cannot access headers!';
	}

	if (!requestHeaders.Authorization) {
		return 'Error: No permissions to access City Health API';
	}

	let paramedUrl = resolveBaseURL(endpoint, 'update');

	let resolvedFilters = Object.keys(filters).map((filterKey) => {
		return `${filterKey}=${filters[filterKey]}`;
	});
	paramedUrl += resolvedFilters.length ? 
		'?' + resolvedFilters.join('&') :
		'';

	let response = await axios.post(paramedUrl, data, options);
	return response;
};

const remove = async (endpoint, filters, data) => {
	let requestHeaders = headers();
	let options = {
		headers: requestHeaders
	};

	if (!requestHeaders) {
		return 'Error: Cannot access headers!';
	}

	if (!requestHeaders.Authorization) {
		return 'Error: No permissions to access City Health API';
	}

	let paramedUrl = resolveBaseURL(endpoint, 'remove');

	let resolvedFilters = Object.keys(filters).map((filterKey) => {
		return `${filterKey}=${filters[filterKey]}`;
	});

	paramedUrl += resolvedFilters.length ?
		'?' + resolvedFilters.join('&') :
		'';

	let response = await axios.post(paramedUrl, data, options);
	return response;
};

const helpers = {
	get_pk,
	filter,
	create,
	update,
	remove
};

export default helpers;