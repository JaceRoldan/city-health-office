import Vue from "vue"
import Vuex from "vuex"

import user from "./modules/user.store";
import app from "./modules/app.store";
import main from "./main";


Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		user,
		app,
		main
	},
})



