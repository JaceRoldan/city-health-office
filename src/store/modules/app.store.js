export default {
    namespaced: true,
    state: {
        alert: {
            show: false,
            message: "",
            type: "success"
        },
        loading: false
    },
    mutations: {
        'SET_ALERT'(state, data) {
            Object.assign(state.alert, {...data, show: true})

            setTimeout(() => {
                state.alert.alert = "";
                state.alert.type = "success";
                state.alert.show = false;
            }, 5000)
        }
    },
    actions: {
        setAlert({commit}, data) {
            commit('SET_ALERT', data)
        }
    }
}
