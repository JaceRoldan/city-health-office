export default {
    namespaced: true,

    state: {
        authenticated: false,
        account: {
            email: "",
            first_name: "",
            last_name: "",
            groups: [],
            last_login: null,
            date_joined: null,
        },
    },

    mutations: {
        'SET_ACCOUNT'(state, data) {
            state.account = data;
        },
        "SET_AUTH"(state, data) {
            state.authenticated = data;
        }

    },

    actions: {
        setAccount({commit}, data) {
            commit('SET_ACCOUNT', data);
        },

        setAuth({commit}, data) {
            commit("SET_AUTH", data)
        }
    },

    getters: {
        isAdmin(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "admin")
        },

        isDiagnostic(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "diagnostic")
        },

        isApplicant(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "applicant")
        },
        isInspector(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "inspector")
        },

        isPhysician(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "physician")
        },


        isTreasury(state) {
            if (!state.account)
                return false;
            return state.account.groups.some(item => item.name.toLowerCase() === "treasury")
        },


        justApplicant(state) {
            if (!state.account)
                return false;
            return state.account.groups.every(item => item.name.toLowerCase() === "applicant")
        },

        justPhysician(state) {
            if (!state.account)
                return false;
            return state.account.groups.every(item => item.name.toLowerCase() === "physician")
        },

        justDiagnostic(state) {
            if (!state.account)
                return false;
            return state.account.groups.every(item => item.name.toLowerCase() === "diagnostic")
        },

        justTreasury(state) {
            if (!state.account)
                return false;
            return state.account.groups.every(item => item.name.toLowerCase() === "treasury")
        },


        justInspector(state) {
            if (!state.account)
                return false;
            return state.account.groups.every(item => item.name.toLowerCase() === "inspector")
        }
    }
}
