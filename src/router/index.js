import Vue from "vue";
import Router from "vue-router";
import applicationsRoute from "./applications.route"
import homeRoute from "./home.route"
import settingsRoute from "./settings.route"
import userManagementRoute from "./userManagement.route"
import diagnosticsRoute from "./diagnostics.route"
import treasuryRoute from "./treasury.route"
import {isAuthenticated} from "@/helpers/auth.helper";
import {get} from "lodash";
import store from "@/store";


Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        homeRoute,
        settingsRoute,
        applicationsRoute,
        userManagementRoute,
        diagnosticsRoute,
        treasuryRoute,
        {
            path: '*',
            component: () => import('../pages/404.vue'),
        }
    ]
});

router.beforeEach(async (to, from, next) => {
    const authMeta = get(to.meta, "auth", true);
    const guestMeta = get(to.meta, "guest", false)
    const groupMeta = get(to.meta, "group", [])
    const authenticated = await isAuthenticated();


    if (authMeta && !guestMeta) {
        if (!authenticated) next({name: "landing"});
    } else if (authMeta === false && authenticated) {
        next({name: "landing"});
    }

    if (groupMeta.length) {
        next(groupMeta.some(item => store.getters[`user/${item}`]) ? undefined : 'page-not-found')
    }

    next();
});

export default router;
