export default {
    path: '/back-office',
    component: () => import('../pages/back-office/BackOfficeDashboard.vue'),
    children: [
        {
            path: '',
            name: 'back-office-main',
            component: () => import ('../pages/back-office/BackOfficeMainView.vue'),
        }, {
            path: 'login',
            name: 'back-office-login',
            component: () => import('../pages/back-office/BackOfficeLogin.vue'),
            meta: {
                auth: false,
                guest: true
            }
        }, {
            path: 'applications',
            name: 'back-office-applications',
            component: () => import('../pages/back-office/BackOfficeApplications.vue'),
        }, {
            path: 'sms-notifications',
            name: 'back-office-sms-notifications',
            component: () => import('../pages/back-office/BackOfficeSMSNotificationsPage.vue')
        }, {
            path: 'user-management',
            name: 'back-office-user-management',
            component: () => import('../pages/back-office/UserManagement.vue'),
        }, {
            path: 'application-detail/:application_type/:permit_id',
            name: 'back-office-application-detail',
            component: () => import('../pages/back-office/BackOfficeApplicationDetailPage.vue'),
        }, {
            path: 'logs-management',
            name: 'back-office-logs-management',
            component: () => import('../pages/back-office/BackOfficeLogsManagementPage.vue'),
        }, {
            path: 'master-pages',
            name: 'back-office-master-pages',
            component: () => import('../pages/back-office/BackOfficeMasterPage.vue'),
        }, {
            path: 'regulatory-documents',
            name: 'back-office-regulatory-documents',
            component: () => import('../pages/back-office/master-pages/BackOfficeRegulatoryDocuments.vue'),
        }, {
            path: 'business-categories',
            name: 'back-office-business-categories',
            component: () => import('../pages/back-office/master-pages/BackOfficeBusinessCategories.vue'),
        }, {
            path: 'regulatory-per-business',
            name: 'back-office-regulatories-per-business',
            component: () => import('../pages/back-office/master-pages/BackOfficeRegulatoriesPerBusiness.vue'),
        }, {
            path: 'role-management',
            name: 'back-office-role-management',
            component: () => import('../pages/back-office/BackOfficeRoleManagementPage.vue'),
        }, {
            path: 'software-settings-management',
            name: 'back-office-software-settings-management',
            component: () => import('../pages/back-office/BackOfficeSoftwareSettingsPage.vue'),
        }
    ]
}
