export default {
    path: '',
    component: () => import('../pages/index'),
    children: [
        {
            path: '',
            name: 'landing',
            meta: {guest: true},
            component: () => import('../pages/home/LandingPage.vue')
        },
        {
            path: "register/:type",
            name: "register",
            meta: {auth: false},
            component: () => import('../pages/home/Register.vue'),
        },
        {
            path: "register/confirm/:key",
            name: "register-confirm",
            meta: {auth: false},
            component: () => import('../pages/home/RegisterConfirm.vue'),
        },
        {
            path: "auth/lost-password/:key",
            name: "reset-password",
            meta: {auth: false},
            component: () => import('../pages/home/ResetPassword.vue')
        }
    ]
}
