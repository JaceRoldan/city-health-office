export default {
    path: "/settings",
    component: () => import('../pages/settings/index'),
    children: [
        {
            path: '',
            name: 'settings',
            meta: {tabIndex: 0},
            component: () => import('../pages/settings/Profile'),
        },
        {
            path: 'regulatory-documents',
            name: 'regulatory-documents',
            meta: {tabIndex: 1},
            component: () => import('../pages/settings/RegulatoryDocuments'),
        },
        {
            path: 'business-classifications',
            name: 'business-classifications',
            meta: {tabIndex: 2},
            component: () => import('../pages/settings/BusinessClassifications'),
        }
    ]
}
