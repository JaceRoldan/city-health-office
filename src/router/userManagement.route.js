export default {
    path: "/user-management",
    name: "user-management",
    meta: {group: ["isAdmin"]},
    component:() => import('../pages/UserManagement'),
}
