export default {
    path: '/applications',
    meta: {group: ["isAdmin", "isApplicant", "isInspector"]},
    component: () => import('../pages/applications/ApplicantDashboard.vue'),
    children: [
        {
            path: '',
            name: 'applicant-dashboard',
            meta: {group: ["isAdmin", "isApplicant", "isPhysician", "isInspector"]},
            component: () => import('../pages/applications/Applications.vue'),
        }, {
            path: 'sms',
            name: 'applicant-sms',
            meta: {group: ["isAdmin", "isApplicant"]},
            component: () => import('../pages/applications/ApplicantSMSList.vue'),
        }, {
            path: 'options',
            name: 'applicant-options',
            meta: {group: ["isAdmin", "isApplicant"]},
            component: () => import('../pages/applications/ApplicantApplicationsOptions.vue'),
        }, {
            path: 'health-clearance',
            name: 'health-clearance-apply',
            meta: {group: ["isAdmin", "isApplicant"]},
            component: () => import('../pages/applications/HealthClearance.vue'),
        }, {
            path: 'health-clearance/:id',
            name: 'health-clearance-detail',
            meta: {group: ["isAdmin", "isApplicant", "isPhysician"]},
            component: () => import('../pages/applications/HealthClearance.vue'),
        }, {
            path: 'sanitary-permit',
            name: 'sanitary-permit-apply',
            meta: {group: ["isAdmin", "isApplicant"]},
            component: () => import('../pages/applications/sanitary-permit/SanitaryPermit.vue'),
        }, {
            path: 'sanitary-permit/:id',
            component: () => import('../pages/applications/sanitary-permit/index.vue'),
            children: [
                {
                    path: "",
                    name: 'sanitary-permit-detail',
                    meta: {group: ["isAdmin", "isApplicant", "isInspector"]},
                    component: () => import('../pages/applications/sanitary-permit/SanitaryPermit.vue'),
                },
                {
                    path: "employees",
                    name: 'permit-employees',
                    meta: {group: ["isAdmin", "isApplicant"]},
                    component: () => import('../pages/applications/sanitary-permit/Employees'),
                },

            ],
        }, {
            path: 'special-permit',
            name: 'special-permit-apply',
            meta: {"special-permit": true, group: ["isAdmin", "isApplicant"]},

            component: () => import('../pages/applications/HealthClearance.vue'),
        }, {
            path: 'special-permit/:id',
            name: 'special-permit-detail',
            meta: {"special-permit": true, group: ["isAdmin", "isApplicant", "isPhysician"]},
            component: () => import('../pages/applications/HealthClearance.vue'),
        },
    ]
}
