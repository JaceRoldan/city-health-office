export default {
    path: "/treasury",
    name: "treasury",
    meta: {group: ["isAdmin", "isTreasury"]},
    component: () => import('../pages/Treasury'),
}
