export default {
    path: "/diagnostics",
    name: "diagnostics",
    meta: {group: ["isAdmin", "isDiagnostic"]},
    component: () => import('../pages/diagnostics/index'),
}
