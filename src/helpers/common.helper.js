export function jsonToFormData(data = {}) {
    const formData = new FormData();
    Object.keys(data).forEach((item) => {
        if (data[item] !== null) formData.append(item, data[item]);
    });
    return formData;
}

export function booleanQP(data) {
    try {
        return JSON.parse(data)
    } catch (e) {
        return false;
    }

}
