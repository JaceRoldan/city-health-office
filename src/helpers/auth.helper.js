import {axiosInstance} from "@/services/base";
import {postRefreshTokenQry, postLogoutQry} from "@/services/auth.service";


export function getAuthSession() {
    const auth_data = localStorage.getItem('auth_data');
    if (auth_data) {
        return JSON.parse(auth_data);
    } else {
        return {}
    }
}


export function setAuthSession(data, remember) {
    const minToHour = data.expires_in / 60 / 60;
    const expiration = new Date();
    expiration.setHours(expiration.getHours() + minToHour);

    data.expires_in = expiration;
    data.remember = remember;
    updateAuthSession(data);
}

export const authUser = {
    set: (data) => localStorage.setItem("auth_user", JSON.stringify(data)),
    get: () => JSON.parse(localStorage.getItem("auth_user"))
}

function updateAuthSession(data) {
    const updatedAuth = Object.assign(getAuthSession(), data);
    localStorage.setItem('auth_data', JSON.stringify(updatedAuth));
    Object.assign(axiosInstance.defaults, {headers: {"Authorization": `Bearer ${getAuthSession().access_token}`}});
}

export async function isAuthenticated() {
    const authSession = getAuthSession();
    if (authSession['access_token']) {
        const getExpiration = authSession['expires_in'];
        const dateNow = new Date();
        if (dateNow < new Date(getExpiration)) {
            return true
        } else {
            if (authSession['remember']) {
                try {
                    const refreshData = await postRefreshTokenQry(authSession['refresh_token']);
                    updateAuthSession(refreshData.data);
                    return true
                } catch (e) {
                    return true
                }

            }
        }
    }
    return false
}

export function logout() {
    return postLogoutQry()
        .then(() => {
            localStorage.removeItem('auth_data');
            localStorage.removeItem('auth_user');
        })
}
