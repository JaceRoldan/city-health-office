export const PERMIT_STATUSES = [
    {text: "Pending", value: "pending"},
    {text: "Processing", value: "processing"},
    {text: "Rejected", value: "rejected"},
    {text: "Approved", value: "approved"},
]


export const PERMIT_TYPES = [
    {text: 'Sanitary Permit', value: 'sanitary-permit'},
    {text: 'Health Clearance', value: 'health-clearance'},
    {text: 'Special Permit', value: 'special-permit'}
]
