export const commonMixin = {
    methods: {
        resetData(vm, data_name) {
            vm[data_name] = vm.$options.data.apply(vm)[data_name]
        },
    },
};
