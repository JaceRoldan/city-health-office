import Vue from 'vue';
import App from './App.vue';
import router from './router/index';
import store from './store';
import auth from './utils/auth.js';
import Vuelidate from 'vuelidate'

import {BootstrapVue, IconsPlugin} from 'bootstrap-vue';
import AppFilter from "@/plugins/appFilter.plugin"

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import "@/assets/css/index.scss"
import {commonMixin} from "@/plugins/common.mixins";

Vue.config.productionTip = false

// Install BootstrapVue
Vue.use(AppFilter);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.mixin(commonMixin);

new Vue({
    router,
    store,
    auth,
    render: h => h(App),
}).$mount('#app')
