import {axiosInstance} from "./base"

export function postBusinessQry(data) {
    return axiosInstance.post(`api/businesses/`, data)
}

export function postFileQry(data, params) {
    return axiosInstance.post(`api/files/`, data, {params})
}

export function getBusinessClassificationsQry(params) {
    return axiosInstance.get(`api/business-classifications/`, {params})
}

export function postBusinessClassificationQry(data) {
    return axiosInstance.post(`api/business-classifications/`, data)
}

export function putBusinessClassificationQry(id, data) {
    return axiosInstance.put(`api/business-classifications/${id}/`, data)
}

export function deleteBusinessClassificationQry(id) {
    return axiosInstance.delete(`api/business-classifications/${id}/`)
}

export function getRegulatoryDocumentsQry(params) {
    return axiosInstance.get(`api/regulatory-documents/`, {params})
}

export function postRegulatoryDocumentsQry(data) {
    return axiosInstance.post(`api/regulatory-documents/`, data)
}

export function deleteRegulatoryDocumentsQry(id) {
    return axiosInstance.delete(`api/regulatory-documents/${id}/`)
}

export function putRegulatoryDocumentsQry(id, data) {
    return axiosInstance.put(`api/regulatory-documents/${id}/`, data)
}
