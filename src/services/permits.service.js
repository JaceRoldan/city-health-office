import {axiosInstance} from "./base"


export function getPermitsQry(params) {
    return axiosInstance.get(`api/permits/`, {params})
}

export function getPermitQry(id, params) {
    return axiosInstance.get(`api/permits/${id}/`, {params})
}

export function postPermitQry(data) {
    return axiosInstance.post(`api/permits/`, data)
}

export function putPermitQry(id, data, params) {
    return axiosInstance.patch(`api/permits/${id}/`, data, {params})
}

export function patchPermitQry(id, data, params) {
    return axiosInstance.patch(`api/permits/${id}/`, data, {params})
}

export function deletePermitQry(id) {
    return axiosInstance.delete(`api/permits/${id}/`,)
}

export function getSanitaryPermitQry(permitId, params) {
    return axiosInstance.get(`api/sanitary-permits/`, {params})
}

export function postAttachmentQry(permit, data) {
    return axiosInstance.post(`api/permits/${permit}/attachments/`, data)
}

export function patchAttachmentQry(permit, id, data) {
    return axiosInstance.patch(`api/permits/${permit}/attachments/${id}/`, data)
}

export function postPrintPermitsQry(data) {
    return axiosInstance.post(`api/print-permits/`, data)
}
