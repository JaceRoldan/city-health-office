import {axiosInstance, clientGrant, clientId, clientSecret} from "./base"
import {getAuthSession} from "@/helpers/auth.helper";
import {jsonToFormData} from "@/helpers/common.helper";


export function postUserOTPQry(data) {
    return axiosInstance.post("api/user/otp/", data);
}

export function postLoginQry(data) {
    const loginData = jsonToFormData({
        'grant_type': clientGrant,
        'client_id': clientId,
        'client_secret': clientSecret,
        'username': data.email,
        'password': data.password,
        "user_type": data.user_type,
        "otp": data.otp
    });
    return axiosInstance.post("o/token/", loginData);
}

export async function postRefreshTokenQry(refresh_token) {
    const refreshTokenData = jsonToFormData({
        'grant_type': "refresh_token",
        'client_id': clientId,
        'client_secret': clientSecret,
        'refresh_token': refresh_token,
    });

    return await axiosInstance.post("o/token/", refreshTokenData);
}

export function postLogoutQry() {
    let logoutData = jsonToFormData({
        'grant_type': "refresh_token",
        'client_id': clientId,
        'client_secret': clientSecret,
        'token': getAuthSession().access_token,
    });

    return axiosInstance.post("o/revoke_token/", logoutData);
}

export function getAccountQry(headers) {
    return axiosInstance.get(`api/account/`, {headers})
}

export function putAccountQry(data) {
    return axiosInstance.put(`api/account/`, data)
}

export function postRegistrationQry(data) {
    return axiosInstance.post("api/user/registration/", data)
}

export function getConfirmRegistrationQry(key) {
    return axiosInstance.get(`api/user/registration/${key}/`)
}

export function putConfirmRegistrationQry(key, data) {
    return axiosInstance.put(`api/user/registration/${key}/`, data)
}

export function postLostPasswordQry(data) {
    return axiosInstance.post(`api/user/reset-password/`, data)
}

export function getLostPasswordQry(key) {
    return axiosInstance.get(`api/user/reset-password/${key}/`)
}

export function putLostPasswordQry(key, data) {
    return axiosInstance.put(`api/user/reset-password/${key}/`, data)
}

export function postChangePasswordQry(data) {
    return axiosInstance.put(`api/user/change-password/`, data)
}



