import axios from "axios"
import {getAuthSession} from "@/helpers/auth.helper";
import * as qs from "qs";

axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'XCSRF-TOKEN';

export const clientId = process.env.VUE_APP_AUTH2_CLIENT_ID;
export const clientSecret = process.env.VUE_APP_AUTH2_CLIENT_SECRET;
export const clientGrant = process.env.VUE_APP_AUTH2_GRANT_TYPE;

export const axiosInstance = axios.create({
	baseURL: process.env.VUE_APP_API_URL,
	paramsSerializer: params => qs.stringify(params, {arrayFormat: 'repeat'})
});

if (Object.keys(getAuthSession()).length) {
	const headers = {"Authorization": `Bearer ${getAuthSession().access_token}`};
	Object.assign(axiosInstance.defaults, {headers});
}
