import {axiosInstance} from "./base"


export function getUsersQry(params) {
    return axiosInstance.get(`api/users/`, {params});
}

export function postUserQry(data) {
    return axiosInstance.post(`api/users/`, data);
}

export function putUserQry(id, data) {
    return axiosInstance.put(`api/users/${id}/`, data);
}

export function deleteUserQry(id) {
    return axiosInstance.delete(`api/users/${id}/`);
}

export function getApplicantsQry(params) {
    return axiosInstance.get(`api/applicants/`, {params});
}

export function getApplicantQry(id) {
    return axiosInstance.get(`api/applicants/${id}/`);
}

export function postApplicantQry(data) {
    return axiosInstance.post(`api/applicants/`, data);
}

export function putApplicantQry(id, data) {
    return axiosInstance.put(`api/applicants/${id}/`, data);
}

export function deleteApplicantQry(id) {
    return axiosInstance.delete(`api/applicants/${id}/`);
}

export function postConcernQry(data) {
    return axiosInstance.post('api/user/send-concern/', data);
}

export function getGroupsQry(){
    return axiosInstance.get("api/groups/")
}
