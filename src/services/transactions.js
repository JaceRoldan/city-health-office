import {axiosInstance} from "./base"


export function getTransactionsQry(params) {
    return axiosInstance.get(`api/transactions/`, {params});
}

export function postTransactionsQry(data) {
    return axiosInstance.post(`api/transactions/`, data);
}

export function putTransactionsQry(id, data) {
    return axiosInstance.put(`api/transactions/${id}/`, data);
}
